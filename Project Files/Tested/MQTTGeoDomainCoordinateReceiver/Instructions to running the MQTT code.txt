1- install the MQTTBox application for windows in the link below: 
http://workswithweb.com/html/mqttbox/installing_apps.html

2- click on create MQTT client 
    - name it whatever you want 
    - set the protocol field to mqtt/tcp 
    - change the host to broker.mqtt-dashboard.com 
    - save changes 
    - under the topic to publish write the topic the nodeMCU is subscribed to which is "CurfewProject" 
    - leave all other options as the same 
    - under payload type either A, B, C, D, E, F, G, H followed by the GPS coordinate (example: A24.201200 or B45.200323, etc...)
    - publish the payload 

additional libraries will have to installed in arduino to allow the MQTT functions to work 
-in the arduino IDE go to sketch --> include library --> add .zip library and install the pubsubclient.zip library included 

3- compile and run 