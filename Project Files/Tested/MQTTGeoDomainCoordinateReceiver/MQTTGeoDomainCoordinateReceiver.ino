//This piece of code allows the NodeMCU to listen to messages that are sent on the CurfewProject published channel that we created on MQTTBox 
//the NodeMCU is subscribed to that channel, depending on the starting character of the message sent in that channel, the NodeMCU will store it in a particular String variable 
// Axx.xxxxxx --> north west latitude 
// Bxx.xxxxxx --> north west longitude 
// Cxx.xxxxxx --> north east latitude 
// Dxx.xxxxxx --> north east longitude 
// Exx.xxxxxx --> south west latitude 
// Fxx.xxxxxx --> south west longitude 
// Gxx.xxxxxx --> south east latitude 
// Hxx.xxxxxx --> south east longitude 

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
const char* ssid = "SSID";
const char* password = "password";
const char* mqtt_server = "broker.mqtt-dashboard.com";
String NWLAT, NWLOG, NELAT, NELOG, SELAT, SELOG, SWLAT, SWLOG; 
String latitude, longitude; //values read from the gps are to be stored here 
char charPayload;
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {

  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.println();
  if ((char)payload[0] == 'A') {
    NWLAT = (char*)payload;
    NWLAT = NWLAT.substring(1,10);
  } else if((char)payload[0] == 'B'){
    NWLOG = (char*)payload; 
    NWLOG = NWLOG.substring(1,10); 
  }
  else if((char)payload[0] == 'C'){
    NELAT = (char*)payload; 
    NELAT = NELAT.substring(1,10); 
  }
  else if((char)payload[0] == 'D'){
    NELOG = (char*)payload; 
    NELOG = NELOG.substring(1,10); 
  }
  else if((char)payload[0] == 'E'){
    SWLAT = (char*)payload; 
    SWLAT = SWLAT.substring(1,10); 
  }
  else if((char)payload[0] == 'F'){
    SWLOG = (char*)payload; 
    SWLOG = SWLOG.substring(1,10); 
  }
  else if((char)payload[0] == 'G'){
    SELAT = (char*)payload; 
    SELAT = SELAT.substring(1,10); 
  }
  else if((char)payload[0] == 'H'){
    SELOG = (char*)payload; 
    SELOG = SELOG.substring(1,10); 
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.subscribe("CurfewProject");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
    Serial.print("North west latitude: ");
    Serial.println(NWLAT);
    Serial.print("North west longitude: "); 
    Serial.println(NWLOG);
    Serial.print("North east latitude: "); 
    Serial.println(NELAT); 
    Serial.print("North east longitude: "); 
    Serial.println(NELOG); 
    Serial.print("South west latitude: "); 
    Serial.println(SWLAT); 
    Serial.print("South west longitude: "); 
    Serial.println(SWLOG); 
    Serial.print("South east latitude: "); 
    Serial.println(SELAT); 
    Serial.print("South east longitude: "); 
    Serial.println(SELOG); 
    geoDomainTest();
}
void geoDomainTest(){
    if( latitude <= NWLAT and latitude >= SWLAT and longitude >= NWLOG and longitude <= SELOG ) {
        Serial.print("Within geolocation domain");
        delay(500); 
      }
    else{
        Serial.print("out of geolocation domain");
        delay(500); 
      }
   Serial.println();
      }